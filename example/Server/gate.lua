local Gate = {}
local moveTime = 2000
local gateDistance = 5

function Gate:new(modelId, position, rotation, moveFactor, owner)
	this = setmetatable({}, {__index = self})
	this.__index = self
	this.modelId = modelId
    this.position = position
    this.rotation = rotation
    this.isUsed = false
    this.moveUp = true
    this.moveFactor = moveFactor
    this.owner = owner
    this.timer = nil
    this.object = Object(modelId, position, rotation)
	return this
end

local GatesTable = {
    [0] = Gate:new(988, Vector3(2488.3000488281, -1667.1999511719, 13.10000038147), Vector3(0, 0, 0), 7, "Tester"),
    [1] = Gate:new(975, Vector3(2469.3999023438, -1660.5999755859, 14), Vector3(0, 0, 0), 4, "Tester")
}

function Gate:getOwner() return self.owner end
function Gate:getPosition() return self.position end


function Gate:move()
    if (self.isUsed) then
        self.object:stop()
        self.timer:destroy()
    end
    if (self.moveUp) then
        self.object:move(moveTime, self.position.x, self.position.y, self.position.z - self.moveFactor)
        self.moveUp = false
    elseif not self.moveUp then
        self.object:move(moveTime, self.position.x, self.position.y, self.position.z)
        self.moveUp = true
    end

    self.isUsed = true
    self.timer = Timer(
    function()
        self.isUsed = false
    end, moveTime, 1)  
end
 
local function IsGateInDistance(player)
    for key, gate in pairs(GatesTable) do
		local distance = getDistanceBetweenPoints3D(player:getPosition(), gate:getPosition())
        if distance <= gateDistance then
            if(gate:getOwner() == player:getName()) then
			    gate:move()
            end
		end
	end
end

addCommandHandler ( "gate", 
function(source)
    IsGateInDistance(source)
end)


addEventHandler("onPlayerJoin", root,
function ()
    bindKey (source, "H", "down", 
        function(source)
            IsGateInDistance(source)
        end)
end)